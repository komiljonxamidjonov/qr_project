from django.contrib import admin
from .models import Family, FamilyMember, Mahalla, MemberType

admin.site.register(Family)
admin.site.register(FamilyMember)
admin.site.register(Mahalla)
admin.site.register(MemberType)
