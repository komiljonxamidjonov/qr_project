from django.db import models
import qrcode
from io import BytesIO
from django.core.files import File
from PIL import Image, ImageDraw
from config.settings import SITE_PATH
# from location_field.models.plain import PlainLocationField

class Mahalla(models.Model):
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title


class Family(models.Model):
    title = models.CharField(max_length=255)
    mahalla = models.ForeignKey(Mahalla, null=True, on_delete=models.SET_NULL)
    address = models.CharField(max_length=255)
    address_link = models.CharField(max_length=500, blank=True, default="https://www.google.com/maps/place/Manzil/@41.3150439,69.2752998,15z/data=!4m5!3m4!1s0x0:0x7d66e01f5e19a1f7!8m2!3d41.3157128!4d69.2792713")
    qr_code = models.ImageField(null=True, blank=True)

    # location = PlainLocationField(based_fields=['city'], zoom=7)
    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        text = f"{SITE_PATH}/family/detail/{self.id}"
        print(text)
        qr_code = qrcode.make(text)
        qr_offset = Image.new('RGB', (400, 400), 'white')
        draw_img = ImageDraw.Draw(qr_offset)
        qr_offset.paste(qr_code)
        file_name = f'{self.title}-{self.id}qr.png'
        stream = BytesIO()
        qr_offset.save(stream, 'PNG')
        self.qr_code.save(file_name, File(stream), save=False)
        qr_offset.close()
        super().save(*args, **kwargs)


class MemberType(models.Model):
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title


class FamilyMember(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    family = models.ForeignKey(Family, null=True, on_delete=models.SET_NULL)
    member_type = models.ForeignKey(MemberType, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"
