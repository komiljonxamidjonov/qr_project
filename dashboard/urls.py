from django.urls import path
from .views import *
urlpatterns = [
    # path('',main_dashboard,name="main_dashboard"),
    path('login/', login_page, name="login_page"),
    path('logout/', logout_page, name="logout_page"),

    path('', family_list, name="family_list"),
    path('family/create/', family_create, name="family_create"),
    path('family/<int:pk>/edit/', family_edit, name='family_edit'),
    path('family/<int:pk>/delete/', family_delete, name='family_delete'),

    path('family-member/create/<int:family_id>/', family_member_create, name='family_member_create'),
    path('family-member/edit/<int:family_id>/<int:pk>/', family_member_edit, name='family_member_edit'),
    path('family/detail/<int:pk>/', family_detail, name="family_detail"),
    path('family-member/<int:pk>/delete/', family_member_delete, name='family_member_delete'),

]
