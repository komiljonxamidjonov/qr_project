from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render

from . import forms
from .models import Family, FamilyMember
from config.settings import SITE_PATH

def login_required_decarator(func):
    return login_required(func, login_url='login_page')


def login_page(request):
    if request.POST:
        username = request.POST.get("username", None)
        password = request.POST.get("password", None)
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('family_list')
    return render(request, 'dashboard/login.html')


@login_required_decarator
def logout_page(request):
    logout(request)
    return redirect('login_page')

@login_required_decarator
def family_list(request):
    families = Family.objects.all()
    ctx = {
        'families': families
    }
    return render(request, "dashboard/family/list.html", ctx)

@login_required_decarator
def family_detail(request, pk):
    family = Family.objects.get(pk=pk)
    family_members = FamilyMember.objects.filter(family_id=pk)
    ctx = {
        'family_members': family_members,
        'family': family
    }
    return render(request, "dashboard/family/detail.html", ctx)


@login_required_decarator
def family_create(request):
    model = Family()
    form = forms.FamilyForm(request.POST or None, request.FILES or None, instance=model)
    ctx={}
    if request.POST and form.is_valid():

        form.save()
        return redirect('family_list')
    ctx['model'] = model
    ctx["form"] = form
    return render(request, 'dashboard/family/form.html', ctx)

@login_required_decarator
def family_member_create(request, family_id):
    family = Family.objects.get(pk=family_id)
    model = FamilyMember()
    form = forms.FamilyMemberForm(request.POST or None, instance=model)
    if request.POST and form.is_valid():
        member = form.save(commit=False)
        member.family = family
        member.save()
        return redirect('family_detail', pk=family.pk)
    ctx = {
        'model': model,
        'form': form
    }
    return render(request, 'dashboard/family/member_form.html', ctx)

@login_required_decarator
def family_edit(request, pk):
    model = Family.objects.get(pk=pk)
    form = forms.FamilyForm(request.POST or None, request.FILES or None, instance=model)

    if request.POST and form.is_valid():
        form.save()
        return redirect('family_list')
    ctx = {
        'model': model,
        'form': form
    }
    return render(request, 'dashboard/family/form.html', ctx)

@login_required_decarator
def family_member_edit(request, family_id, pk):
    model = FamilyMember.objects.get(pk=pk)
    family = get_object_or_404(Family, pk=family_id)
    form = forms.FamilyMemberForm(request.POST or None, request.FILES or None, instance=model)
    if request.POST and form.is_valid():
        member = form.save(commit=False)
        member.family = family
        member.save()
        return redirect('family_detail', pk=family_id)
    ctx = {
        'model': model,
        'form': form
    }
    return render(request, 'dashboard/family/member_form.html', ctx)

@login_required_decarator
def family_delete(request, pk):
    model = Family.objects.get(pk=pk)
    model.delete()
    return redirect("family_list")

@login_required_decarator
def family_member_delete(request, pk):
    model = FamilyMember.objects.get(pk=pk)
    family = model.family
    model.delete()
    print(family.id)
    return redirect("family_detail", pk=family.pk)
