from django import forms
from .models import Family, FamilyMember, Mahalla


class FamilyForm(forms.ModelForm):
    class Meta:
        model = Family
        fields = "__all__"
        widgets = {
            "title": forms.TextInput(attrs={'class': 'form-control'}),
            "address": forms.TextInput(attrs={'class': 'form-control'}),
            "mahalla": forms.Select(attrs={'class': 'form-control'}),
            "qr_code": forms.FileInput(attrs={
                'class': 'form-control',

                'onchange': 'loadFile(event)'
            }
            ),
        }


class FamilyMemberForm(forms.ModelForm):
    class Meta:
        model = FamilyMember
        fields = ('first_name', 'last_name', 'member_type')
        widjets = {
            "first_name": forms.TextInput(attrs={'class': 'form-control'}),
            "last_name": forms.TextInput(attrs={'class': 'form-control'}),
            "member_type": forms.Select(attrs={'class': 'form-control'}),

        }
